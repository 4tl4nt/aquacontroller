#include "Timer.h"

const TimePoint TimePoint::MAX(23, 59, 59);
const TimePoint TimePoint::DAY(24, 0, 0);
const TimePoint TimePoint::BAD(24, 0, 1);

Timer::Timer(std::vector<Period> *d, TimePoint *currentTime, std::function<void(bool)> f)
  : m_durations(d)
  , m_currentTime(currentTime)
  , m_functor(f)
{
}

TimePoint Timer::getNextTime()
{
  auto result = (*m_durations)[0].to;
  for (auto d : *m_durations)
  {
    if(d == *m_currentTime)
    {
      if (d.to != TimePoint::MAX)
        result = d.to;
      break;
    }
  }
  return result;
}

bool Timer::getCurrentState()
{
  bool result;
  for (auto d : *m_durations)
  {
    if(d == *m_currentTime)
    {
      result = d.state;
      break; 
    }
  }
  return result;
}

void Timer::update()
{
  for (auto d : *m_durations)
  {
    if(d == *m_currentTime)
    {
      m_functor(d.state);
    }
  }
}
