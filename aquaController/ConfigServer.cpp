#include "ConfigServer.h"

#include <SoftwareSerial.h>
#include <EEPROM.h>

#define START_MAGIC_NUMBER 0b11010111U

uint32_t crc32(uint32_t crc, void *buf, unsigned long len)
{
  uint8_t *p = (uint8_t *)buf;
  uint32_t res = ~crc;
  while (len--)
  {
    uint8_t curr = *p++;
    uint32_t rem = (res & 0xFF) ^ curr;
    for (int i = 0; i < 8; i++)
    {
      rem >>= 1;
      if (rem & 1)
      {
        rem^= 0xEDB88320UL;
      }
    }
    res = (res >> 8) ^ rem;
  }
  return ~res;
}

void ConfigServer::begin()
{
  EEPROM.begin(512);
      delay(50);
}

WifiMode ConfigServer::getWifiMode()
{
  return m_config.wifiMode;
}

void ConfigServer::setWifiMode(WifiMode wm)
{
  m_config.wifiMode = wm;
}

int ConfigServer::getWifiReconnectTries()
{
  return m_config.wifiReconnectTries;
}

void ConfigServer::setWifiReconnectTries(int count)
{
  m_config.wifiReconnectTries = count;
}

/*uint8_t ConfigServer::getLcdBrightness()
{
  return m_config.lcdBrightness;
}

void ConfigServer::setLcdBrightness(uint8_t brightness)
{
  m_config.lcdBrightness = brightness;
}*/

std::vector<Period> *ConfigServer::getLightTiming()
{
  return &m_config.lights;
}

void ConfigServer::setLightTiming(const std::vector<Period> &timings)
{
  m_config.lights = timings;
}

std::vector<Period> *ConfigServer::getAirTiming()
{
  return &m_config.airs;
}

void ConfigServer::setAirTiming(const std::vector<Period> &timings)
{
  m_config.airs = timings;
}

void ConfigServer::save()
{
  size_t addr = 0;
  uint32_t crc = 0;
  size_t size;
  uint8_t *p;

  EEPROM.write(addr++, START_MAGIC_NUMBER);

  size = sizeof(m_config.wifiMode);
  p = (uint8_t *)&m_config.wifiMode;
  crc = crc32(crc, p, size);
  for (size_t i = addr; i < size + addr; i++)
  {
    EEPROM.write(i, *p++);
  }
  addr += size;
  
  size = sizeof(m_config.wifiReconnectTries);
  p = (uint8_t *)&m_config.wifiReconnectTries;
  crc = crc32(crc, p, size);
  for (size_t i = addr; i < size + addr; i++)
  {
    EEPROM.write(i, *p++);
  }
  addr += size;
  
  /*size = sizeof(m_config.lcdBrightness);
  p = (uint8_t *)&m_config.lcdBrightness;
  crc = crc32(crc, p, size);
  for (size_t i = addr; i < size + addr; i++)
  {
    EEPROM.write(i, *p++);
  }
  addr += size;*/

  for (const auto &light : m_config.lights)
  {
    size = sizeof(light);
    p = (uint8_t *)&light;
    crc = crc32(crc, p, size);
    for (size_t i = addr; i < size + addr; i++)
    {
      EEPROM.write(i, *p++);
    }
    addr += size;
  }

  for (const auto &air : m_config.airs)
  {
    size = sizeof(air);
    p = (uint8_t *)&air;
    crc = crc32(crc, p, size);
    for (size_t i = addr; i < size + addr; i++)
    {
      EEPROM.write(i, *p++);
    }
    addr += size;
  }

  size = sizeof(crc);
  p = (uint8_t *)&crc;
  for (size_t i = addr; i < size + addr; i++)
  {
    EEPROM.write(i, *p++);
  }
  m_config.crc32 = crc;

  EEPROM.commit();
}

void ConfigServer::restore()
{
  size_t addr = 0;
  uint32_t crc = 0;
  size_t size;
  uint8_t *p;

  if (EEPROM.read(addr++) == START_MAGIC_NUMBER)
  {
    size = sizeof(m_config.wifiMode);
    p = (uint8_t *)&m_config.wifiMode;
    for (size_t i = 0; i < size; i++)
    {
      *(p+i) = EEPROM.read(i + addr); 
    }
    crc = crc32(crc, p, size);
    addr += size;
    
    size = sizeof(m_config.wifiReconnectTries);
    p = (uint8_t *)&m_config.wifiReconnectTries;
    for (size_t i = 0; i < size; i++)
    {
      *(p+i) = EEPROM.read(i + addr); 
    }
    crc = crc32(crc, p, size);
    addr += size;
    
    /*size = sizeof(m_config.lcdBrightness);
    p = (uint8_t *)&m_config.lcdBrightness;
    for (size_t i = 0; i < size; i++)
    {
      *(p+i) = EEPROM.read(i + addr); 
    }
    crc = crc32(crc, p, size);
    addr += size;*/

    for (auto &light : m_config.lights)
    {
      size = sizeof(light);
      p = (uint8_t *)&light;
      for (size_t i = 0; i < size; i++)
      {
      *(p+i) = EEPROM.read(i + addr); 
      }
      crc = crc32(crc, p, size);
      addr += size;
    }

    for (auto &air : m_config.airs)
    {
      size = sizeof(air);
      p = (uint8_t *)&air;
      for (size_t i = 0; i < size; i++)
      {
      *(p+i) = EEPROM.read(i + addr); 
      }
      crc = crc32(crc, p, size);
      addr += size;
    }

    size = sizeof(m_config.crc32);
    p = (uint8_t *)&m_config.crc32;
    for (size_t i = 0; i < size; i++)
    {
      *(p+i) = EEPROM.read(i + addr); 
    }
    if (crc != m_config.crc32)
    {
      Serial.println("Invalid CRC32. Restored default config");
      defaultConfig();
    }
  }
  else
  {
    Serial.println("Invalid magic number. Restored default config");
    defaultConfig();
  }
    
}

void ConfigServer::defaultConfig()
{
  m_config.wifiMode = CLIENT;
  setWifiReconnectTries();
  //m_config.lcdBrightness = 255;

    m_config.lights[0] = {TimePoint( 0, 0, 0), TimePoint( 7, 0, 0),  false};
    m_config.lights[1] = {TimePoint( 7, 0, 0), TimePoint(22, 0, 0),  true};
    m_config.lights[2] = {TimePoint(22, 0, 0), TimePoint::MAX,       false};
    m_config.lights[3] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.lights[4] = {TimePoint::MAX,      TimePoint::MAX,       false};
    m_config.lights[5] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.lights[6] = {TimePoint::MAX,      TimePoint::MAX,       false};
    m_config.lights[7] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.lights[8] = {TimePoint::MAX,      TimePoint::MAX,       false};

    m_config.airs[0] = {TimePoint( 0, 0, 0), TimePoint( 0,30, 0),  false};
    m_config.airs[1] = {TimePoint( 0,30, 0), TimePoint( 8, 0, 0),  true};
    m_config.airs[2] = {TimePoint( 8, 0, 0), TimePoint::MAX,       false};
    m_config.airs[3] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.airs[4] = {TimePoint::MAX,      TimePoint::MAX,       false};
    m_config.airs[5] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.airs[6] = {TimePoint::MAX,      TimePoint::MAX,       false};
    m_config.airs[7] = {TimePoint::MAX,      TimePoint::MAX,       true};
    m_config.airs[8] = {TimePoint::MAX,      TimePoint::MAX,       false};

  save();
}
