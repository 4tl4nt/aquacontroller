#pragma once

#include <vector>
#include <functional>
#include <cstdint>

#include <SoftwareSerial.h>

class TimePoint
{
public:
  //TimePoint& operator=(const TimePoint&) = default;
  //TimePoint(const TimePoint &) = default;
  TimePoint() : TimePoint(0) {}
  TimePoint(uint32_t sec) : TimePoint(sec / 60, sec % 60) {}
  TimePoint(uint32_t min, uint16_t sec) : TimePoint(min / 60, min % 60, sec) {}
  TimePoint(uint16_t hour, uint16_t min, uint16_t sec)
    : m_hours(hour)
    , m_minutes(min)
    , m_seconds(sec) {}

  bool operator>(TimePoint right) const {return this->toSeconds() > right.toSeconds();}
  bool operator<(TimePoint right) const {return this->toSeconds() < right.toSeconds();}
  bool operator>=(TimePoint right) const {return this->toSeconds() >= right.toSeconds();}
  bool operator<=(TimePoint right) const {return this->toSeconds() <= right.toSeconds();}
  bool operator==(TimePoint right) const {return this->toSeconds() == right.toSeconds();}
  bool operator!=(TimePoint right) const {return this->toSeconds() != right.toSeconds();}
  //TimePoint operator+(int i) const {TimePoint res(this->toSeconds() + i); return res;}
  operator uint32_t() const {return toSeconds();}
  uint32_t toSeconds() const {return uint32_t(m_hours) * 60 * 60 + m_minutes * 60 + m_seconds;}

  uint16_t getSeconds() const {return m_seconds;}
  uint16_t getMinutes() const {return m_minutes;}
  uint16_t getHours() const {return m_hours;}

  static const TimePoint MAX;
  static const TimePoint DAY;
  static const TimePoint BAD;
private:
  uint16_t m_hours;
  uint16_t m_minutes;
  uint16_t m_seconds;
};

class Period
{
public:
  TimePoint from;
  TimePoint to;
  bool state;
  bool operator==(TimePoint right) {return this->from <= right && this->to >= right;}
};

class Timer
{
public:
  Timer(std::vector<Period> *durations, TimePoint *currentTime, std::function<void(bool)> functor);
  ~Timer() = default;
  TimePoint getNextTime();
  bool getCurrentState();
  void update();

private:
  std::vector<Period> *m_durations;
  TimePoint *m_currentTime;
  std::function<void(bool)> m_functor;
};
