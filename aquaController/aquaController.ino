#include <vector>
#include <functional>
#include <cstdint>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <Wire.h>
#include <LiquidCrystal_PCF8574.h>
#include <DallasTemperature.h>
#include <OneWire.h>
#include <TZ.h>
#include <time.h>
#include <Schedule.h>

#include "config.h"
#include "MainWindow.h"
#include "Timer.h"
#include "TickerScheduler.h"
#include "ConfigServer.h"
#include "WiFiHelper.h"

#define MYTZ TZ_Europe_Kiev

LiquidCrystal_PCF8574 lcd(0x27);
OneWire oneWire(TEMP_PIN);
DallasTemperature sensor(&oneWire);
ConfigServer configServer;
TimePoint timeNow;
WiFiHelper wiFiHelper(&configServer);
MainWindow mainWindow(&timeNow);
TickerScheduler mainTickerScheduler(TASK_MAX_ID);

void setupNTP()
{
  Serial.println("Syncing time...");
  configTime(MYTZ, "pool.ntp.org");
  const auto ntpWaitingFreq = 100;
  for(int i = 0; time(nullptr) < 1000000000ul && i < (NTP_CONN_TIMEOUT * 1000); i += ntpWaitingFreq)
  {
    Serial.print('.');
    delay(ntpWaitingFreq);
  }
  Serial.println();
}

void setupOTA()
{
  Serial.println("Init OTA...");
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
}

void setupScheduler()
{
  mainTickerScheduler.add(TASK_COLON_BLINK, FREQ_COLON_BLINK, [](void*)
  {
    mainWindow.blinkColon();
  }, NULL, true);

  mainTickerScheduler.add(TASK_TIME_UPDATE, FREQ_TIME_UPDATE, [](void*)
  {
    auto now = time(nullptr);
    struct tm *tmp = localtime(&now);
    timeNow = TimePoint(tmp->tm_hour, tmp->tm_min, tmp->tm_sec);
  }, NULL, true);

  mainTickerScheduler.add(TASK_TEMP_UPDATE, FREQ_TEMP_UPDATE, [](void*)
  {
    sensor.requestTemperatures();
    float temperature = sensor.getTempCByIndex(0);
    mainWindow.setTemperature(temperature);
  }, NULL, true);

  mainTickerScheduler.add(TASK_STATE_UPDATE, FREQ_STATE_UPDATE, [](void*)
  {
    static Timer lightScheduler(configServer.getLightTiming(), &timeNow, [](bool state)
    {
      if (digitalRead(LIGHT_PIN) != state)
      {
        digitalWrite(LIGHT_PIN, state);
      }
    });

    static Timer airScheduler(configServer.getAirTiming(), &timeNow, [](bool state)
    {
      if (digitalRead(AIR_PIN) == state)
      {
        digitalWrite(AIR_PIN, !state);
      }
    });
    lightScheduler.update();
    airScheduler.update();
    mainWindow.setNextLightSwitch(lightScheduler.getNextTime());
    mainWindow.setNextAirSwitch(airScheduler.getNextTime());
    mainWindow.setLightState(lightScheduler.getCurrentState());
    mainWindow.setAirState(airScheduler.getCurrentState());
  }, NULL, true);

  mainTickerScheduler.add(TASK_MAIN_WINDOW_UPDATE, FREQ_MAIN_WINDOW_UPDATE, [](void*)
  {
    mainWindow.update();
  }, NULL, true);

  mainTickerScheduler.add(TASK_FISH_UPDATE, FREQ_FISH_UPDATE, [](void*)
  {
    mainWindow.updateFish();
  }, NULL);
}

void setup() {  
  // Init Pinouts
  pinMode(RED_LED_PIN, OUTPUT);
  pinMode(BLUE_LED_PIN, OUTPUT);
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(AIR_PIN, OUTPUT);
  digitalWrite(RED_LED_PIN, HIGH);
  digitalWrite(BLUE_LED_PIN, HIGH);
  digitalWrite(LIGHT_PIN, LOW);
  digitalWrite(AIR_PIN, LOW);
  pinMode(BUTTON_PIN, INPUT_PULLUP);

  // Init serial
  Serial.begin(115200);

  // Init DS 18b20
  Serial.println("Init DS18B20...");
  sensor.begin();
  sensor.setResolution(12);
  sensor.setWaitForConversion(false);

  // Init LCD
  Serial.println("Init LCD...");
  Wire.begin();
  Wire.beginTransmission(0x27);
  auto ret = Wire.endTransmission();
  if (ret == 0) {
    Serial.println(": LCD found.");
    lcd.begin(20, 4);
  } else {
    Serial.print("ERROR: Wire return code: ");
    Serial.println(ret);
    Serial.println("ERROR: LCD not found.");
  }
  lcd.setBacklight(255); // TODO bug on start
  lcd.home();
  lcd.print("AQUA  CONTROLLER");
  lcd.setCursor(0, 1);
  lcd.print("v: ");
  lcd.print(VERSION_MAJOR);
  lcd.print(".");
  lcd.print(VERSION_MINOR);

  // Init EEPROM
  Serial.println("Init EEPROM...");
  configServer.begin();
  configServer.restore();

  if(wiFiHelper.init())
  {
    if (wiFiHelper.isConnected())
    {
      setupNTP();
      setupOTA();
    }
  }

  setupScheduler();
  mainWindow.init();
  Serial.println(" Done!");
}

void loop() {
  mainTickerScheduler.update();
  if (wiFiHelper.isConnected()) ArduinoOTA.handle();
}
