#include "MainWindow.h"
#include <SoftwareSerial.h>

#include <LiquidCrystal_PCF8574.h>

extern LiquidCrystal_PCF8574 lcd;

namespace
{

void printNum(int col, int row, size_t size, char fill, int num)
{
  bool positiveNum = num >= 0 ? true : false;
  auto n = abs(num);
  for (size_t i = 1; i <= size; i++)
  {
    lcd.setCursor(col + size - i, row);
    if (n > 0)
    {
      lcd.print(n % 10);
      n /= 10;
    }
    else
    {
      lcd.print(fill);  
    }
  }
  if (num == 0)
  {
    lcd.setCursor(col + size - 1, row);
    lcd.print(0);
  }
}

void printTime(int col, int row, const TimePoint &timePoint)
{
  printNum(col, row, 2, ' ', timePoint.getHours());
  printNum(col + 3, row, 2, '0', timePoint.getMinutes());
}

  byte lightOnCharacter[] = {
  B01110,
  B00100,
  B01010,
  B01010,
  B10001,
  B10101,
  B00100,
  B00100
};

  byte airOnCharacter[] = {
  B00001,
  B10100,
  B00010,
  B01001,
  B10010,
  B01010,
  B01100,
  B00000
};

  byte fishHeadLeft[] = {
  B00011,
  B00111,
  B01111,
  B11011,
  B01111,
  B00111,
  B00001,
  B00000
};

  byte fishBodyLeft[] = {
  B10000,
  B11000,
  B11110,
  B11111,
  B11111,
  B11110,
  B11000,
  B00000
};

  byte fishEndLeft[] = {
  B00011,
  B00110,
  B01110,
  B11100,
  B11100,
  B01110,
  B00110,
  B00011
};

  byte fishHeadRight[] = {
  B11000,
  B11100,
  B11110,
  B11011,
  B11110,
  B11100,
  B10000,
  B00000
};

  byte fishBodyRight[] = {
  B00001,
  B00011,
  B01111,
  B11111,
  B11111,
  B01111,
  B00011,
  B00000
};

  byte fishEndRight[] = {
  B11000,
  B01100,
  B01110,
  B00111,
  B00111,
  B01110,
  B01100,
  B11000
};

}

MainWindow::MainWindow(TimePoint *currentTime)
  : m_temperature(-128.0)
  , m_displayedTime(TimePoint::BAD)
  , m_currentTime(currentTime)
  , m_lightState(-1)
  , m_airState(-1)
  , m_nextLightSwitch(TimePoint::BAD)
  , m_nextAirSwitch(TimePoint::BAD)
  , m_fishPos(0)
  , m_fishDirection(true)
  , m_backlight(true)
{
}
void MainWindow::init()
{
  lcd.clear();
  lcd.createChar(0, lightOnCharacter);
  lcd.createChar(1, airOnCharacter);
  lcd.createChar(2, fishHeadLeft);
  lcd.createChar(3, fishBodyLeft);
  lcd.createChar(4, fishEndLeft);
  lcd.createChar(5, fishHeadRight);
  lcd.createChar(6, fishBodyRight);
  lcd.createChar(7, fishEndRight);
  lcd.setCursor(16, 0);
  lcd.print(":");
  lcd.setCursor(16, 3);
  lcd.print(":");
  lcd.setCursor(0, 1);
  for (int i=0; i<20; i++)
  {
    lcd.print('-');
  }
}
void MainWindow::updateFish()
{
  if (m_backlight)
  {
    static unsigned int steps = random(0, 15);
    lcd.setCursor(m_fishPos, 2);
    lcd.print("   ");
    if (m_fishDirection)
    {
      if (++m_fishPos > 16 || --steps == 0)
      {
        m_fishDirection = false;
        steps = random(m_fishPos)+2;
      }
      lcd.setCursor(m_fishPos, 2);
      lcd.write(7);
      lcd.write(6);
      lcd.write(5);
    }
    else
    {
      if (--m_fishPos == 0 || --steps == 0)
      {
        m_fishDirection = true;
        steps = random(15 - m_fishPos)+2;
      }
      lcd.setCursor(m_fishPos, 2);
      lcd.write(2);
      lcd.write(3);
      lcd.write(4);
    }
  }
}

void MainWindow::update()
{
  if (m_displayed)
  {
    if (m_displayedTime.getMinutes() != m_currentTime->getMinutes() ||
        m_displayedTime.getHours() != m_currentTime->getHours())
    {
      m_displayedTime = *m_currentTime;
      printTime(0, 0, m_displayedTime);
    }
  }
}

void MainWindow::setTemperature(float temp)
{
  if (m_displayed)
  {
    if (temp != m_temperature)
    {
      m_temperature = temp;
      lcd.setCursor(0, 3);
      lcd.print(m_temperature);
      lcd.setCursor(5, 3);
      lcd.print("  ");
    }
  }
}

void MainWindow::setLightState(int state)
{
  if (m_displayed)
  {
    if (m_lightState != state)
    {
      m_lightState = state;
      lcd.setCursor(13, 0);
      if (m_lightState)
      {
        lcd.write(0);
        enableBacklight(true);
      }
      else
      {
        lcd.print(' ');
        enableBacklight(false);
      }
    }
  }
}

void MainWindow::setAirState(int state)
{
  if (m_displayed)
  {
    if (m_airState != state)
    {
      m_airState = state;
      lcd.setCursor(13, 3);
      if (m_airState)
        lcd.write(1);
      else
        lcd.print('.');
    }
  }
}

void MainWindow::setNextLightSwitch(const TimePoint &timePoint)
{
  if (m_displayed)
  {
    if (m_nextLightSwitch != timePoint)
    {
      m_nextLightSwitch = timePoint;
      printTime(14, 0, m_nextLightSwitch);
    }
  }
}

void MainWindow::setNextAirSwitch(const TimePoint &timePoint)
{
  if (m_displayed)
  {
    if (m_nextAirSwitch != timePoint)
    {
      m_nextAirSwitch = timePoint;
      printTime(14, 3, m_nextAirSwitch);
    }
  }
}

void MainWindow::blinkColon()
{
  static bool state = false;
  lcd.setCursor(2, 0);
  if (state)
    lcd.print(":");
  else
    lcd.print(" ");
  state = !state;
}

void MainWindow::enableBacklight(bool state)
{
  m_backlight = state;
  lcd.setBacklight(state ? 255 : 0);
}
