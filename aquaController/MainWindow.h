#pragma once

#include <cstdint>
#include "Timer.h"

class Window
{
public:
  Window() : m_displayed(-1) {};
  virtual ~Window() = default;
  virtual void update() = 0;

protected:
  bool m_displayed;
};

class MainWindow : public Window
{
public:
  MainWindow(TimePoint *currentTime);
  virtual ~MainWindow() = default;
  void init();
  void setTemperature(float temp);
  void setLightState(int state);
  void setAirState(int state);
  void setNextLightSwitch(const TimePoint &timePoint);
  void setNextAirSwitch(const TimePoint &timePoint);
  void updateFish();
  void blinkColon();
  void enableBacklight(bool state);
  virtual void update() override;

private:
  float m_temperature;
  TimePoint m_displayedTime;
  TimePoint *m_currentTime;
  int m_lightState;
  int m_airState;
  TimePoint m_nextLightSwitch;
  TimePoint m_nextAirSwitch;
  int m_fishPos;
  bool m_fishDirection;
  bool m_backlight;
};
