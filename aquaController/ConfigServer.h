#pragma once

#include <vector>

#include "Timer.h"
#include "config.h"

enum WifiMode
{
  AP,
  CLIENT,
  DISCONNECTED
};

class ConfigServer
{
  struct Config
  {
    Config() : lights(PERIOD_VECTOR_SIZE), airs(PERIOD_VECTOR_SIZE) {}
    WifiMode wifiMode;
    int wifiReconnectTries;
    //uint8_t lcdBrightness;
    std::vector<Period> lights;
    std::vector<Period> airs;
    uint32_t crc32;
  };
public:
  ConfigServer() = default;
  void begin();
  WifiMode getWifiMode();
  void setWifiMode(WifiMode mode);
  int getWifiReconnectTries();
  void setWifiReconnectTries(int count = WIFI_RECONNECT_TRIES);
  //uint8_t getLcdBrightness();
  //void setLcdBrightness(uint8_t brightness);
  std::vector<Period> *getLightTiming();
  void setLightTiming(const std::vector<Period> &timings);
  std::vector<Period> *getAirTiming();
  void setAirTiming(const std::vector<Period> &timings);
  void save();
  void restore();
  void defaultConfig();

private:
  Config m_config;
};
