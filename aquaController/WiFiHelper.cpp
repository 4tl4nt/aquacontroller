#include "WiFiHelper.h"

#include <WiFiManager.h> // worked with 0.15.0 version

WiFiManager wifiManager;
WiFiServer wifiServer(ESP_HTTP_PORT);
bool configurationPageShown(false);

namespace
{
    void apCallback(WiFiManager *wifiManager)
    {
        Serial.println("Configuration page was shown");
        configurationPageShown = true;
    }
}

WiFiHelper::WiFiHelper(ConfigServer *configServer)
    : m_configServer(configServer), m_wifiMode(WifiMode::DISCONNECTED)
{
}

WiFiHelper::~WiFiHelper()
{
}

bool WiFiHelper::init()
{
    Serial.println("Init WiFi");
    wifiManager.setDebugOutput(true);
    wifiManager.setAPCallback(apCallback);
    wifiManager.setConnectTimeout(ESP_CONN_TIMEOUT);
    wifiManager.setConfigPortalTimeout(ESP_CONF_TIMEOUT);

    m_wifiMode = m_configServer->getWifiMode();
    Serial.print("WiFi mode: ");
    Serial.println(m_wifiMode);
    if (m_wifiMode == WifiMode::AP)
    {
        WiFi.softAP(DEFAULT_AP_NAME, DEFAULT_AP_PASS);
        Serial.println("Started in access point mode");
        wifiServer.begin();
    }
    else
    {
        if (WiFi.SSID().length())
        {
            Serial.print("Previously connected to WiFi network: ");
            Serial.println(WiFi.SSID());
            WiFi.mode(WIFI_STA);
            WiFi.begin();
            WiFi.waitForConnectResult();
            if (!WiFi.isConnected())
            {
                auto tries = m_configServer->getWifiReconnectTries();
                if(tries > 0)
                {
                    Serial.print("Cannot connect to previous network, tries: ");
                    Serial.println(tries);
                    m_configServer->setWifiReconnectTries(tries - 1);
                    m_configServer->save();
                    ESP.restart();
                }
                else
                {
                    m_configServer->setWifiReconnectTries();
                    m_configServer->save();
                    Serial.println("Starting config portal");
                    wifiManager.startConfigPortal(DEFAULT_AP_NAME, DEFAULT_AP_PASS);
                }
            }
            else
            {
                m_configServer->setWifiReconnectTries();
                m_configServer->save();
            }
        }
        else
        {
            Serial.println("WiFi network is not defined. Configuration proccess will started...");
            wifiManager.setBreakAfterConfig(true);
            wifiManager.autoConnect(DEFAULT_AP_NAME, DEFAULT_AP_PASS);
        }
        if (!WiFi.isConnected())
        {
            Serial.println("WiFi is not connected");
            if (configurationPageShown)
            {
                if (millis() < (ESP_CONN_TIMEOUT + ESP_CONF_TIMEOUT) * 1000U)
                {
                    Serial.println("Failed connection to WiFi network");
                    Serial.println("Make sure the WiFi network name and password are correct");
                    Serial.println("Restart to request a new WiFi network name and password");
                    wifiManager.resetSettings();
                    ESP.restart();
                }
                else
                {
                    Serial.println("Waiting time for entering SSID and password from WiFi network or connecting to WiFi network is exceeded");
                }
            }
            else
            {
                Serial.println("Failed connection to WiFi network");
                Serial.println("Perhaps the specified WiFi network is no longer available");
            }
        }
    }
    Serial.print("Connected to WiFi: ");
    Serial.println(WiFi.SSID());
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    return true;
}

bool WiFiHelper::isConnected()
{
    return m_wifiMode != WifiMode::DISCONNECTED;
}
