#ifndef MYCONFIG_H
#define MYCONFIG_H

#include "Timer.h"

#define VERSION_MAJOR 0
#define VERSION_MINOR 9

#define RED_LED_PIN D0
#define BLUE_LED_PIN D4
#define LCD_SCL_PIN D1
#define LCD_SDA_PIN D2
#define LIGHT_PIN D3
#define AIR_PIN D6
#define TEMP_PIN D5
#define RESERVED D7
#define BUTTON_PIN D8

#define TASK_LED_BLINK 1
#define FREQ_LED_BLINK 200
#define TASK_INPUT (TASK_LED_BLINK + 1)
#define FREQ_INPUT 50
#define TASK_UPTIME (TASK_INPUT + 1)
#define FREQ_UPTIME 1000
#define TASK_COLON_BLINK (TASK_INPUT + 1)
#define FREQ_COLON_BLINK 500
#define TASK_TIME_UPDATE (TASK_COLON_BLINK + 1)
#define FREQ_TIME_UPDATE 1000
#define TASK_TEMP_UPDATE (TASK_TIME_UPDATE + 1)
#define FREQ_TEMP_UPDATE 5000
#define TASK_STATE_UPDATE (TASK_TEMP_UPDATE + 1)
#define FREQ_STATE_UPDATE 1000
#define TASK_MAIN_WINDOW_UPDATE (TASK_STATE_UPDATE + 1)
#define FREQ_MAIN_WINDOW_UPDATE 1000
#define TASK_FISH_UPDATE (TASK_MAIN_WINDOW_UPDATE + 1)
#define FREQ_FISH_UPDATE 399
#define TASK_MAX_ID (TASK_FISH_UPDATE + 1)

#define PERIOD_VECTOR_SIZE 9

#define DEFAULT_AP_NAME "aquaController"
#define DEFAULT_AP_PASS "12345679" 

#define WIFI_RECONNECT_TRIES  (3)  

#define ESP_CONN_TIMEOUT      (7U)                          // время в секундах (ДОЛЖНО БЫТЬ МЕНЬШЕ 8, иначе сработает WDT), которое ESP будет пытаться подключиться к WiFi сети, после его истечения автоматически развернёт WiFi точку доступа
#define ESP_CONF_TIMEOUT      (300U)                        // время в секундах, которое ESP будет ждать ввода SSID и пароля WiFi сети роутера в конфигурационном режиме, после его истечения ESP перезагружается
#define ESP_HTTP_PORT         (80U)                         // номер порта, который будет использоваться во время первой утановки имени WiFi сети (и пароля), к которой потом будет подключаться лампа в режиме WiFi клиента (лучше не менять)
#define NTP_CONN_TIMEOUT      (10U)


#endif
