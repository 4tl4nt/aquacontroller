#pragma once

#include "ConfigServer.h"

class WiFiHelper
{
private:
    ConfigServer *m_configServer;
    bool m_configurationPageShown;
    enum WifiMode m_wifiMode;

public:
    WiFiHelper(ConfigServer *configServer);
    ~WiFiHelper();

public:
    bool init();
    bool isConnected();
};
